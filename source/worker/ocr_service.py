from io import BytesIO

import pytesseract
import requests
from PIL import Image
from worker.constants import ALLOWED_EXTENSIONS
from worker.exceptions import FileExtensionIsNotSupportedException
from worker.utils import is_allowed_file


class OcrService:
    """
    Service to process images by url.
    """

    def __init__(self, file_url):
        self.file_url = file_url

    def __load_image(self) -> Image:
        """
        Load image object.
        :return: Image object.
        :exception: requests.exceptions.ConnectionError.
        """
        response = requests.get(self.file_url)
        return Image.open(BytesIO(response.content))

    def process(self) -> str:
        """
        Process text from image by url.
        :exception: FileExtensionIsNotSupportedException, requests.exceptions.ConnectionError
        """
        if not is_allowed_file(self.file_url):
            raise FileExtensionIsNotSupportedException(
                f"Supported extensions: {ALLOWED_EXTENSIONS}"
            )
        with self.__load_image() as img:
            text = pytesseract.image_to_string(img)
            return text

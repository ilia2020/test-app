from worker.constants import ALLOWED_EXTENSIONS


def is_allowed_file(filename: str) -> bool:
    """
    Check file format. I know we can manage better :-)
    :param str filename: path to file.
    :return: True if file format is legal.
    :rtype: bool
    """
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS

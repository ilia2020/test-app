from http import HTTPStatus


def test_json_data_ok(client):
    """
    Case: make post request with correct data.
    Expect: HTTPStatus.OK response.
    """
    response = client.post(
        "api/tasks", json={"fileUrl": "http://jeroen.github.io/images/testocr.png"}
    )
    assert response.status_code == HTTPStatus.OK.value
    assert response.data.decode() == HTTPStatus.OK.phrase


def test_json_data_bad(client):
    response = client.post(
        "api/tasks", json={"bad_key": "http://jeroen.github.io/images/testocr.png"}
    )
    assert response.status_code == HTTPStatus.BAD_REQUEST.value
    assert response.data.decode() == "Missing key: 'fileUrl'"

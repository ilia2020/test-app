import pytest
from worker.constants import ALLOWED_EXTENSIONS
from worker.utils import is_allowed_file


@pytest.mark.parametrize("file_extention", ALLOWED_EXTENSIONS)
def test_is_allowed_file(file_extention):
    assert is_allowed_file(".".join(["filename", file_extention]))


@pytest.mark.parametrize("file_extention", ["pdf", "mp3"])
def test_is_not_allowed_file(file_extention):
    assert not is_allowed_file(".".join(["filename", file_extention]))

import pytest
import requests
from worker.exceptions import FileExtensionIsNotSupportedException
from worker.ocr_service import OcrService


def test_wrong_file_format():
    """
    Case: try to process file with wrong format.
    Expect: Raises FileExtensionIsNotSupportedException
    """
    with pytest.raises(FileExtensionIsNotSupportedException):
        OcrService("file.pdf").process()


def test_text_recognition():
    """
    Case: process picture with correct url
    Expect: recognized text
    """
    url = "http://jeroen.github.io/images/testocr.png"
    text = OcrService(url).process()
    assert len(text) == 287
    assert type(text) == str


def test_bad_url():
    """
    Case: try to pass bad url.
    Expect: raises ConnectionError.
    """
    bad_url = "http://jeroen.io/1231_testocr.png"
    with pytest.raises(requests.exceptions.ConnectionError):
        OcrService(bad_url).process()

from unittest.mock import patch

import pytest

from source.tasks import process_ocr


@patch("source.tasks.logger.warning")
@patch("source.tasks.logger.info")
@pytest.mark.parametrize(
    "file_info, warning_called, info_called",
    [
        ({"fileUrl": "http://jeroen.github.io/images/testocr.png"}, 0, 2),
        ({"file_key": "http://jeroen.github.io/images/testocr.png"}, 1, 1),
    ],
)
def test_worker_run(
    log_info_mock, log_warning_mock, file_info, warning_called, info_called, client
):
    """
    Case: We should log exception only in case of the error.
    Expect: We don't log exception if we don't have any.
    """
    process_ocr(file_info=file_info)
    assert log_warning_mock.call_count == warning_called
    assert log_info_mock.call_count == info_called

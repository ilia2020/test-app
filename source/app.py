"""
Flask app.
"""

from flask import Flask
from views import api_bp

app = Flask(__name__)

app.register_blueprint(api_bp, url_prefix="/api")


def create_app():
    return app

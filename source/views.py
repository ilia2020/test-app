from http import HTTPStatus

import tasks
from flask import Blueprint, request
from schema import SchemaError
from schemas import new_file_message_schema

api_bp = Blueprint("api", __name__)


@api_bp.route("/tasks", methods=["POST"])
def add_ocr_task():
    new_ocr_task = request.get_json()
    try:
        data = new_file_message_schema.validate(new_ocr_task)
    except SchemaError as exc:
        return str(exc), HTTPStatus.BAD_REQUEST

    tasks.process_ocr.delay(data)
    return HTTPStatus.OK.phrase


@api_bp.route("/", methods=["GET"])
def index():
    return "hello api!"

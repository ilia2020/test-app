"""
Worker tasks.
"""
import logging

import requests
from celery import Celery
from schema import SchemaError
from schemas import new_file_message_schema
from worker import config
from worker.exceptions import FileExtensionIsNotSupportedException
from worker.ocr_service import OcrService

logger = logging.getLogger(__name__)

celery = Celery(
    "worker",
    broker=config.CELERY_BROKER_URL,
    backend=config.CELERY_BACKEND,
)


@celery.task()
def process_ocr(file_info):
    """
    Run ocr processing for image by url.
    :param dict file_info: file info dict
    :return: Recognized text.
    :rtype: str
    """
    logger.info(f"start processing: {file_info}")
    try:
        validated = new_file_message_schema.validate(file_info)
        url = validated["fileUrl"]
        result = OcrService(url).process()
    except (
        FileExtensionIsNotSupportedException,
        requests.exceptions.ConnectionError,
        SchemaError,
    ) as exc:
        logger.warning(exc)
    except Exception as exc:
        logger.exception(exc)
    else:
        logger.info(f"Result text: {result}")

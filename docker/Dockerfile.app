FROM python:3.9

WORKDIR /source

COPY requirements/base.txt ./requirements.txt

RUN pip install --no-cache-dir -r requirements.txt

COPY /source .

EXPOSE 8080

CMD ["waitress-serve", "--host", "0.0.0.0", "--call", "app:create_app"]
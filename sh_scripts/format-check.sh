#!/bin/bash

isort --profile black --check .; (( exit_status = exit_status || $? ))
black --check .; (( exit_status = exit_status || $? ))

exit $exit_status
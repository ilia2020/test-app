## Intro
Simple app for ocr image processing.
<br>
Main tech stack: `flask`, `celery`, `redis`, `docker`, `pytest`.

## Up project
```
docker-compose up -d --build
```
Navigate browser on `localhost:8080`


## Clean your code before push
```
sh_scripts/format-check.sh
sh_scripts/lint.sh
```

## Manual test
```
curl -d '{"fileUrl":"http://jeroen.github.io/images/testocr.png"}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/tasks
```
Check ocr processing result in worker using:
```
docker logs worker
```

## Auto tests
```
docker-compose -f docker-compose.tests.yml up -d --build
docker exec -it app python -m pytest tests
```

## Check coverage
```
docker exec -it app coverage run -m pytest tests
docker exec -it app coverage report -m
```
Goal is to keep coverage above 30%.
